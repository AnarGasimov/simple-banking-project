package com.payment.service;

import com.payment.entity.Payment;

import java.math.BigDecimal;

public interface PaymentService {
    Payment performTopUp(Long customerId, BigDecimal amount);
    Payment performPurchase(Long customerId, BigDecimal amount);
    Payment performRefund(Long customerId, BigDecimal amount);
}
