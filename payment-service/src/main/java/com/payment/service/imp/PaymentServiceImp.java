package com.payment.service.imp;

import com.payment.entity.Payment;
import com.payment.repository.PaymentRepository;
import com.payment.service.PaymentService;
import com.payment.types.PaymentTypes;
import com.payment.webclient.CustomerServiceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;


@Service
public class PaymentServiceImp implements PaymentService {
    private final PaymentRepository paymentRepository;
    private final CustomerServiceClient customerServiceClient;
    private final Logger logger = LoggerFactory.getLogger(PaymentService.class);

    @Autowired
    public PaymentServiceImp(PaymentRepository paymentRepository, CustomerServiceClient customerServiceClient) {
        this.paymentRepository = paymentRepository;
        this.customerServiceClient = customerServiceClient;
    }

    @Transactional
    public Payment performTopUp(Long customerId, BigDecimal amount) {
        Payment payment = buildPayment(customerId,amount,PaymentTypes.TOP_UP);
        paymentRepository.save(payment);

        BigDecimal newBalance = customerServiceClient.getCustomerBalance(customerId).add(amount);
        customerServiceClient.updateCustomerBalance(customerId, newBalance);

        logger.info("Top-up transaction completed for CustomerId: {}, Amount: {}", customerId, amount);
        return payment;
    }

    @Transactional
    public Payment performPurchase(Long customerId, BigDecimal amount) {
        BigDecimal currentBalance = customerServiceClient.getCustomerBalance(customerId);
        if (currentBalance.compareTo(amount) < 0) {
            logger.error("Insufficient funds for the purchase for CustomerId: {}, Amount: {}", customerId, amount);
            throw new IllegalArgumentException("Insufficient funds for the purchase.");
        }

        Payment payment = buildPayment(customerId,amount,PaymentTypes.PURCHASE);
        paymentRepository.save(payment);

        BigDecimal newBalance = currentBalance.subtract(amount);
        customerServiceClient.updateCustomerBalance(customerId, newBalance);

        logger.info("Purchase transaction completed for CustomerId: {}, Amount: {}", customerId, amount);
        return payment;
    }

    @Transactional
    public Payment performRefund(Long customerId, BigDecimal refundAmount) {
        Payment lastPurchase = paymentRepository.findTopByCustomerIdAndTypeOrderByTransactionDateDesc(customerId, PaymentTypes.PURCHASE);

        if (lastPurchase == null || lastPurchase.getAmount().compareTo(refundAmount) < 0) {
            logger.error("Invalid refund amount for CustomerId: {}, Amount: {}", customerId, refundAmount);
            throw new IllegalArgumentException("Invalid refund amount.");
        }

        Payment payment = buildPayment(customerId, refundAmount, PaymentTypes.REFUND);
        paymentRepository.save(payment);

        BigDecimal newBalance = customerServiceClient.getCustomerBalance(customerId).add(refundAmount);
        customerServiceClient.updateCustomerBalance(customerId, newBalance);

        return payment;
    }

    private Payment buildPayment(Long customerId, BigDecimal amount, String transactionType) {
        return Payment.builder()
                .customerId(customerId)
                .amount(amount)
                .type(transactionType)
                .transactionDate(LocalDateTime.now())
                .build();
    }
}