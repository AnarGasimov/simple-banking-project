package com.payment.controller;

import com.payment.dto.TransactionRequest;
import com.payment.entity.Payment;
import com.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/payments")
public class PaymentController {

    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping("/top-up")
    public ResponseEntity<Payment> topUp(@RequestBody TransactionRequest request) {
        Payment payment = paymentService.performTopUp(request.getCustomerId(), request.getAmount());
        return ResponseEntity.ok(payment);
    }

    @PostMapping("/purchase")
    public ResponseEntity<Payment> purchase(@RequestBody TransactionRequest request) {
        Payment payment = paymentService.performPurchase(request.getCustomerId(), request.getAmount());
        return ResponseEntity.ok(payment);
    }

    @PostMapping("/refund")
    public ResponseEntity<Payment> refund(@RequestBody TransactionRequest request) {
        Payment payment = paymentService.performRefund(request.getCustomerId(), request.getAmount());
        return ResponseEntity.ok(payment);
    }
}
