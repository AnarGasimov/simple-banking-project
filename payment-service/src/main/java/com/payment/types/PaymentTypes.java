package com.payment.types;

public class PaymentTypes {
    public static final String TOP_UP = "TOP_UP";
    public static final String PURCHASE = "PURCHASE";
    public static final String REFUND = "REFUND";
}
