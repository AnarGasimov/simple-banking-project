package com.payment.webclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;

@Component
public class CustomerServiceClient {

    private final RestTemplate restTemplate;
    private final DiscoveryClient discoveryClient;

    @Autowired
    public CustomerServiceClient(RestTemplate restTemplate,DiscoveryClient discoveryClient) {
        this.restTemplate = restTemplate;
        this.discoveryClient = discoveryClient;
    }

    public BigDecimal getCustomerBalance(Long customerId) {
        String url = getCustomerServiceBaseUrl() + "/customers/" + customerId + "/balance";
        return restTemplate.getForObject(url, BigDecimal.class);
    }

    public void updateCustomerBalance(Long customerId, BigDecimal newBalance) {
        String url = getCustomerServiceBaseUrl() + "/customers/" + customerId + "/balance";
        restTemplate.put(url, newBalance);
    }

    public String getCustomerServiceBaseUrl() {
        // Querying Eureka for the customer service instance
        List<ServiceInstance> instances = discoveryClient.getInstances("customer-service");

        if (!instances.isEmpty()) {
            ServiceInstance instance = instances.get(0);
            return instance.getUri().toString();
        } else {
            throw new RuntimeException("Customer service not found in Eureka.");
        }
    }
}
