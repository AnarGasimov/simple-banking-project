package com.customer.mapper;

import com.customer.dto.CustomerRequest;
import com.customer.entities.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "balance", constant = "100.00")
    Customer dtoToEntity(CustomerRequest customerRequest);
}
