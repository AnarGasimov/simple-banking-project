package com.customer.services;

import com.customer.entities.Customer;
import com.customer.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final Logger logger = LoggerFactory.getLogger(CustomerService.class);

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Transactional
    public Customer createCustomer(Customer customer) {
        logger.info("Creating customer: {}", customer);
        customer.setBalance(new BigDecimal("100.0")); // Initial balance set to 100 AZN
        return customerRepository.save(customer);
    }

    @Transactional(readOnly = true)
    public Customer getCustomerById(Long id) {
        logger.info("Retrieving customer with ID: {}", id);
        return customerRepository.findById(id).orElse(null);
    }
    @Transactional
    public void updateCustomerBalance(Long customerId, BigDecimal newBalance) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new IllegalArgumentException("Customer not found with ID: " + customerId));
        customer.setBalance(newBalance);
        customerRepository.save(customer);
    }
    public BigDecimal getCustomerBalance(Long customerId) {
        return customerRepository.findBalanceByCustomerId(customerId);
    }
}
