package com.customer.controllers;

import com.customer.entities.Customer;
import com.customer.services.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private final CustomerService customerService;
    private final Logger logger = LoggerFactory.getLogger(CustomerController.class);

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
        logger.info("Request received to create customer: {}", customer);
        Customer createdCustomer = customerService.createCustomer(customer);
        return ResponseEntity.ok(createdCustomer);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> getCustomer(@PathVariable Long id) {
        logger.info("Request received to get customer with id: {}", id);
        Customer customer = customerService.getCustomerById(id);
        if (customer != null) {
            return ResponseEntity.ok(customer);
        } else {
            logger.error("Customer not found with id: {}", id);
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{customerId}/balance")
    public ResponseEntity<?> updateBalance(@PathVariable Long customerId, @RequestBody BigDecimal newBalance) {
        customerService.updateCustomerBalance(customerId, newBalance);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{customerId}/balance") // Versioned endpoint for getting customer balance
    public ResponseEntity<BigDecimal> getCustomerBalance(@PathVariable Long customerId) {
        BigDecimal balance = customerService.getCustomerBalance(customerId);
        return ResponseEntity.ok(balance);
    }

}
