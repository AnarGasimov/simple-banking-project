package com.customer.dto;

import java.time.LocalDate;

public class CustomerRequest {
    private String name;
    private String surname;
    private LocalDate birthDate;
    private String gsmNumber;
}
