package com.customer.repository;

import com.customer.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query("SELECT c.balance FROM Customer c WHERE c.id = :customerId")
    BigDecimal findBalanceByCustomerId(@Param("customerId") Long customerId);
}
