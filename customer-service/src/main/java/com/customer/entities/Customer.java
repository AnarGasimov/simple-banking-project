package com.customer.entities;


import jakarta.persistence.*;
import jdk.jfr.Name;
import lombok.*;


import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    private LocalDate birthDate;
    private String gsmNumber;
    private BigDecimal balance;
}