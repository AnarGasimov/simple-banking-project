#!/bin/bash

# Function to generate a self-signed SSL certificate
generate_ssl_certificate() {
    if [ ! -f "./api-gateway/src/main/resources/keystore.p12" ]; then
        echo "Generating SSL certificate..."
        keytool -genkeypair -alias myalias -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore api-gateway/src/main/resources/keystore.p12 -validity 3650 -storepass password -dname "CN=localhost, OU=OrgUnit, O=MyOrg, L=Baku, C=AZ"
        echo "SSL certificate generated."
    else
        echo "SSL certificate already exists in the API Gateway resources."
    fi
}

# Generate SSL certificate
generate_ssl_certificate

echo "Building and creating Docker images for the services..."

# Building API Gateway
cd ./api-gateway
mvn clean package -DskipTests
docker build -t api-gateway:latest .

# Building Eureka Server
cd ../service-registry
mvn clean package -DskipTests
docker build -t eureka-server:latest .

# Building Customer Service
cd ../customer-service
mvn clean package -DskipTests
docker build -t customer-service:latest .

# Building Transaction Service
cd ../payment-service
mvn clean package -DskipTests
docker build -t payment-service:latest .

# Starting all services using Docker Compose
cd ..
echo "Starting all services with Docker Compose..."
docker-compose up --build
